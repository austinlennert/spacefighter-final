
#include <string>
#include "MainMenuScreen.h"
#include "GameplayScreen.h"

// Callback Functions
void OnStartGameSelect(MenuScreen *pScreen)
{
	pScreen->GetScreenManager()->AddScreen(new GameplayScreen());
}

void OnQuitSelect(MenuScreen *pScreen)
{
	MainMenuScreen *pMainMenuScreen = (MainMenuScreen *)pScreen;
	pMainMenuScreen->SetQuitFlag();
	pMainMenuScreen->Exit();
}

void OnScreenRemove(Screen *pScreen)
{
	MainMenuScreen *pMainMenuScreen = (MainMenuScreen *)pScreen;
	if (pMainMenuScreen->IsQuittingGame()) pScreen->GetGame()->Quit();
}



MainMenuScreen::MainMenuScreen()
{
	m_pTexture = nullptr;

	SetRemoveCallback(OnScreenRemove);

	SetTransitionInTime(2.0f);
	SetTransitionOutTime(0.5f);

	Show(); // Show the screen
}

void MainMenuScreen::LoadContent(ResourceManager *pResourceManager)
{
	// Logo
	m_pTexture = pResourceManager->Load<Texture>("Textures\\Logo.png");  // Load logo image from Content
	m_texturePosition = Game::GetScreenCenter() - Vector2::UNIT_Y * 150;  // Set position of logo

	// Create the menu items
	const int COUNT = 2;  // Amount of menu options
	MenuItem *pItem;
	Font::SetLoadSize(20, true);
	Font *pFont = pResourceManager->Load<Font>("Fonts\\Ethnocentric.ttf");  // Sets font for menu item

	SetDisplayCount(COUNT);

	enum Items { START_GAME, QUIT };  // Variables to get menu items with ease 
	std::string text[COUNT] = { "Start Game", "Quit" };  // Text for menu items

	for (int i = 0; i < COUNT; i++)  // Loop through an initialize all menu items
	{
		pItem = new MenuItem(text[i]);
		pItem->SetPosition(Vector2(100, 100 + 50 * i));  // Set menu item position
		pItem->SetFont(pFont);
		pItem->SetColor(Color::Blue);
		pItem->SetSelected(i == 0);  // By default set first (index 0) item to be selected item
		AddMenuItem(pItem);  // Add the item to the menu
	}

	GetMenuItem(START_GAME)->SetSelectCallback(OnStartGameSelect);  // Set method to run when item is selected  - Start the game
	GetMenuItem(QUIT)->SetSelectCallback(OnQuitSelect);				//											- Quit the game
}

void MainMenuScreen::Update(const GameTime *pGameTime)
{
	MenuItem *pItem;

	// Set the menu item colors
	for (int i = 0; i < GetDisplayCount(); i++)
	{
		pItem = GetMenuItem(i);
		pItem->SetAlpha(GetAlpha());

		if (pItem->IsSelected()) pItem->SetColor(Color::Green);  // Selected color
		else pItem->SetColor(Color::Red);  // Not-selected color
	}

	MenuScreen::Update(pGameTime);
}

void MainMenuScreen::Draw(SpriteBatch *pSpriteBatch)
{
	pSpriteBatch->Begin();
	pSpriteBatch->Draw(m_pTexture, m_texturePosition, Color::White * GetAlpha(), m_pTexture->GetCenter());
	pSpriteBatch->End();

	MenuScreen::Draw(pSpriteBatch);
}
